#version 120

uniform sampler2DRect tex0;//this is the background texture

uniform vec2 size;

void main(){

	//this is the fragment shader
	//this is where the pixel level drawing happens
	//gl_FragCoord gives us the x and y of the current pixel its drawing
  float Pi = 6.28318530718; // Pi*2

  // GAUSSIAN BLUR SETTINGS {{{
  float Directions = 16.0; // BLUR DIRECTIONS (Default 16.0 - More is better but slower)
  float Quality = 3.0; // BLUR QUALITY (Default 4.0 - More is better but slower)
  float Size = 8.0; // BLUR SIZE (Radius)
  // GAUSSIAN BLUR SETTINGS }}}

  vec2 Radius = Size/size.xy;
	//we grab the x and y and store them in an int

  vec2 uv = gl_FragCoord.xy;

  vec4 Color = texture2DRect(tex0, uv);
  // Color += texture2DRect(tex0, uv+vec2(cos(d),sin(d))*Radius*i);

  // Blur calculations
  for( float d=0.0; d<Pi; d+=Pi/Directions)
    {
      for(float i=1.0/Quality; i<=1.0; i+=1.0/Quality)
        {
          Color += texture2DRect(tex0, uv+vec2(cos(d),sin(d))*Radius*i);
        }
    }

  // Color /= Quality * Directions - 15.0;

  gl_FragColor = Color;

  // gl_FragColor = vec4(1.0, 1.0,1.0,1.0);
}
