#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){

  b_shader = false;

  imgui.setup();

  ImGuiIO& imio = ImGui::GetIO();

  imio.FontGlobalScale = 1.0f;

  line_width = 1.0f;
  pattern_size = 40.0f;
  size_alt_hex = 1.0f;
  ratio_hex = 1.0f;
  current_mode = 2;
  draw_gui = true;

  view_x = ofGetWidth()/2;
  
  draw_debug = false;

  params.noise_scale_angle = 2000.0f;
  params.noise_scale_delta = 2000.0f;
  params.noise_speed = 0;
  params.noise_pos = 0;
  // GUI
  gui.setup("Star Pattern"); // most of the time you don't need a name


  // gui.add(draw_gui.set("draw_gui", false));
  gui.add(draw_pattern.set("draw_pattern", false));
  gui.add(draw_hankin.set("draw_hankin", true));


  // gui.add(pattern_size.set("pattern_size", 100, 10, 100));

  group_mode.setName("pattern");

  group_mode.add(mode_squares.set("squares", false));
  group_mode.add(mode_hexa.set("hexa", false));
  group_mode.add(mode_octo.set("octo", false));

  gui.add(group_mode);

  group.setName("variation");

  group.add(mode_noise.set("noise", true));
  group.add(mode_sine.set("sine", false));

  group.add(mode_manual.set("manual", true));
  group.add(a.set("angle", 0.0, -180.0, 180.0));
  group.add(delta.set("delta", 0.0, -1.0, 1.0));


  gui.add(group);

  gui.add(fps.setup("fps", 0.0, 0.0, 60.0));

  mode_squares.addListener(this, &ofApp::modeChanged);
  mode_hexa.addListener(this, &ofApp::modeChanged);
  mode_octo.addListener(this, &ofApp::modeChanged);

  a.addListener(this, &ofApp::changed);
  delta.addListener(this, &ofApp::deltaChanged);
  // pattern_size.addListener(this, &ofApp::patternChanged);

  sync.setup((ofParameterGroup&)gui.getParameter(),6669,"localhost",6668);

  // ofEnableSmoothing();

  // createSquare();
  // createHex();
  createPattern();
  // patternChanged(pattern_size);
  ofSetLineWidth(0.5);

#ifdef TARGET_OPENGLES
	shader.load("shaders_gles/pat.vert","shaders_gles/pat.frag");
#else
	if(ofIsGLProgrammableRenderer()){
		shader.load("shaders_gl3/pat.vert", "shaders_gl3/pat.frag");
	}else{
		shader.load("shaders/pat.vert", "shaders/pat.frag");
	}
#endif

  do_svg_export = false;
}

ofPolyline ofApp::polyReg(ofPoint center, float radius, int n, float start_angle){
  ofPolyline new_poly;
  float angle = TWO_PI/n;
  for (int i=0; i<n; i++){
    float x = center.x + radius*cos(start_angle-i*angle);
    float y = center.y + radius*sin(start_angle-i*angle);

    new_poly.addVertex(ofVec3f(x, y, 0));
  }
  new_poly.close();

  return new_poly;
}

ofPolyline ofApp::polyHex(ofPoint center, float radius, float e){
  ofPolyline new_poly;
  float angle = TWO_PI/6;

  // float x_max = 1;
  // float x_min = cos(angle);
  // float y_max = sin(angle);

  float mid = (1 + cos(angle))/2;

  // x_min = ofMap(e, 1, 0, x_min, mid);
  // x_max = ofMap(e, 1, 0, x_max, mid);
  // float x_min = mid;
  // float x_max = mid;
  float x_min = ofMap(e, 1.0f, 0.0f, cos(angle), mid);
  float x_max = ofMap(e, 1.0f, 0.0f, 1.0f,       mid);
  float y_max = e * sin(angle);

  new_poly.addVertex(center.x + x_max * radius, center.y);
  new_poly.addVertex(center.x + x_min * radius, center.y - y_max * radius);
  new_poly.addVertex(center.x - x_min * radius, center.y - y_max * radius);

  new_poly.addVertex(center.x - x_max * radius, center.y);
  new_poly.addVertex(center.x - x_min * radius, center.y + y_max * radius);
  new_poly.addVertex(center.x + x_min * radius, center.y + y_max * radius);

  new_poly.close();

  return new_poly;

}

void ofApp::createOcto(){
  float s = pattern_size;
  float o_step = 2*s + glm::sqrt(2)*s; // octo space
  float o_width = (s + 2*glm::sqrt(2)*s)/2; // octo width
  float rad = s / (glm::sqrt((2-glm::sqrt(2)))); // octo rad

  float pattern_scale = o_width + s;

  float n_x = ofGetWidth()/o_step + 3;
  float n_y = ofGetHeight()/o_step + 3;

  float s_square = s/glm::sqrt(2);
  float s_start = o_step/2;
  float step_y = o_step/2;

  float offx = -(n_x * o_step)/2;
  float offy = -(n_y * o_step)/2;

  for (int j=-n_y; j<n_y; j++){
    float y = step_y*j;
    float dec_x = 0;
    if (j%2==0){
      dec_x = step_y;
    }
    // dec_x += offx;
    for (int i=-n_x; i<n_x;i++){
      ofPolyline new_poly;
      if (i%2 == 0){
        float x = o_step*i/2;
        new_poly = polyReg(ofPoint(dec_x + x, y), rad, 8, PI/8);
      }else{
        float x = o_step * i/2;
        new_poly = polyReg(ofPoint(dec_x + x, y), s_square , 4, PI/4);
      }
      polys.push_back(new_poly);

    }
  }

}

void ofApp::createHex(){
  float s = pattern_size * 3;

  float step_x = 3*s/4;
  float step_y = glm::sqrt(3)*s/2;

  float n_x = ofGetWidth()/step_x + 1;
  float n_y = ofGetHeight()/step_y + 1;

  float off_x = -(n_x * step_x)/2;
  float off_y = -(n_y * step_y)/2;

  for (int i =0; i<n_x;i++){
    float x = 3*s * i/4;
    float dec = 0;

    if (i%2==0){
      dec = -glm::sqrt(3)*s/4;
    }

    for (int j=0; j<n_y;j++){
      float y = j * glm::sqrt(3) * s / 2 + dec;
      polys.push_back(polyReg(ofPoint(x+off_x, y+off_y), s/2, 6, 0));
    }
  }
}

void ofApp::createHexSq(){
  float s = pattern_size * 3;

  float step_x = 3*s/2;
  float step_y = (glm::sqrt(3)*s/2) * 3 / 2 ;

  step_y = ofMap(ratio_hex, 1.0, 0.0, step_y, step_x/2);

  float n_x = 2*ceil(ceil(ofGetWidth()/step_x)/2) + 2;
  float n_y = 4*ceil(ceil(ofGetHeight()/step_y)/4) + 4;

  ofLogNotice() << "n " << n_x << " " << n_y;

  float loc_size = size_alt_hex;
  vector<vector<ofPolyline>> polys_pos;
  for (int i = -n_x/2; i< n_x/2; i++){

    vector<ofPolyline> polys_line;
    float x = step_x * i;

    for (int j = -n_y/2; j<n_y/2 ; j++){
      float dec = 0;
      if (j%2==0){
        dec = step_x / 2;
      }

      float y = j * step_y;

      // float loc_size = ofMap(y, 0, 1080, size_alt_hex * 1.2, size_alt_hex * 0.05);
      // float loc_ratio_hex = ofMap(dec+x, 0, 2000, ratio_hex * 1.5, ratio_hex*0.4);
      float loc_ratio_hex = ratio_hex;
      ofPoint pos = ofPoint(dec+x, y);
      ofPolyline new_poly = polyHex(pos, loc_size*s/2, loc_ratio_hex);
      polys.push_back(new_poly);

      poly_centers.push_back(pos);

      polys_line.push_back(new_poly);
    }
    polys_pos.push_back(polys_line);
  }

  for (int i=1; i<n_x-1; i++){
    for (int j=1; j<n_y-1; j++){
      ofPolyline new_poly;
      ofPolyline new_poly2;
      int loc_i = i;
      if (j%2==0){
        loc_i ++;
      }
      new_poly.addVertex(polys_pos[i][j][0]);
      new_poly.addVertex(polys_pos[i][j][5]);
      
      new_poly.addVertex(polys_pos[loc_i][j+1][2]);
      new_poly.addVertex(polys_pos[loc_i][j+1][1]);

      new_poly.addVertex(polys_pos[i+1][j][4]);
      new_poly.addVertex(polys_pos[i+1][j][3]);

      new_poly.close();
      // ofPoint poly_center = (polys_pos[i][j][0] + polys_pos[loc_i][j+1][1])/2;
      poly_centers.push_back(new_poly.getCentroid2D());

      polys.push_back(new_poly);

      new_poly2.addVertex(polys_pos[i][j][5]);
      new_poly2.addVertex(polys_pos[i][j][4]);
      
      new_poly2.addVertex(polys_pos[loc_i-1][j+1][1]);
      new_poly2.addVertex(polys_pos[loc_i-1][j+1][0]);

      new_poly2.addVertex(polys_pos[loc_i][j+1][3]);
      new_poly2.addVertex(polys_pos[loc_i][j+1][2]);

      // ofPoint poly_center_2 = (polys_pos[i][j][5] + polys_pos[loc_i-1][j+1][0])/2;
      poly_centers.push_back(new_poly2.getCentroid2D());

      new_poly2.close();
      polys.push_back(new_poly2);
    }
  }
}

// void ofApp::createPolyHex(ofPoint center, float size){
  
// }

void ofApp::createSquare(){
  float s = pattern_size ;

  int n_x = ofGetWidth()/s +1;
  int n_y = ofGetHeight()/s +1;

  float off_x = -(n_x * s)/2;
  float off_y = -(n_y * s)/2;

  for (int i=0;i<n_x;i++){
    for (int j=0;j<n_y;j++){
      ofPolyline new_poly;
      int dec = 0;
      // if (i%2 == 0){
      //   dec = -s/2;
      // }
      float x1 = i*s + off_x;
      float x2 = i*s + off_x + s;
      float y1 = j*s + off_y + dec;
      float y2 = j*s + off_y + dec + s;
      new_poly.addVertex(ofVec3f(x1, y1, 0));
      new_poly.addVertex(ofVec3f(x1, y2, 0));
      new_poly.addVertex(ofVec3f(x2, y2, 0));
      new_poly.addVertex(ofVec3f(x2, y1, 0));
      new_poly.close();
      polys.push_back(new_poly);
    }
  }
}

void ofApp::createRhombi(){
  float s = pattern_size;

  float step_x = glm::sqrt(3) * s + s;
  float step_y =  s/2 + s + glm::sqrt(3)*s/2;

  float n_x = ofGetWidth()/step_x + 1;
  float n_y = ofGetHeight()/step_y + 1;

  for (int i = -n_x/2; i< n_x/2; i++){

    for (int j = -n_y/2; j<n_y/2 ; j++){
      float dec = 0;
      if (j%2 != 0){
        dec = step_x/2.0f;
      }
      createRhombiCell(i*step_x + dec, j*step_y);
    }
  }
}

void ofApp::createRhombiCell(float pos_x, float pos_y){
  float s = pattern_size;

  float step_y = glm::sqrt(2)*s/2;

  float s_tr = glm::sqrt(3)*s / 3;

  polys.push_back(polyReg(ofPoint(pos_x, pos_y), s, 6, HALF_PI));

  float rad = glm::sqrt(3)*s/2 + s/2;

  float rad_tr = s + s_tr;

  for (int i = 0; i<6; i++){
    float angle = i * TWO_PI/6;
    float x = pos_x + rad * cos(angle);
    float y = pos_y + rad * sin(angle);
    polys.push_back(polyReg(ofPoint(x, y), step_y, 4, angle + HALF_PI/2.0f));

    float angle_tr = angle + PI/6;
    float x_tr = pos_x + rad_tr * cos(angle_tr);
    float y_tr = pos_y + rad_tr * sin(angle_tr);
    polys.push_back(polyReg(ofPoint(x_tr, y_tr), s_tr, 3, angle - PI/6.0f));

  }
}

void ofApp::createPattern(){
  poly_centers.clear();
  polys.clear();

  if (current_mode == 0){
    createSquare();
  }else if(current_mode == 1){
    createHex();
  }else if(current_mode == 2){
    createHexSq();
  }else if(current_mode == 3){
    createOcto();
  }else if(current_mode == 4){
    createRhombi();
  }
  updateHankins();
}

void ofApp::updateHankins(){
  poly_hankins.clear();
  for (int i=0; i< polys.size(); i++){
    PolyHankin poly_hankin = PolyHankin();
    ofPolyline cur_poly = polys[i];

    ofPoint v1 = cur_poly[0] - cur_poly[1];
    ofPoint v2 = cur_poly[2] - cur_poly[1];

    float angle = v1.angleRad(v2);

    if (true || glm::length(cur_poly.getCentroid2D()) < 500){
    // if (cur_poly.getCentroid2D().distance(ofVec2f(ofGetWidth()/2, ofGetHeight()/2)) < 500){
      for (int j =0; j<cur_poly.size();j++){
        Hankin new_hankin = Hankin(cur_poly[j],
                                   cur_poly[(j+1)%cur_poly.size()],
                                   angle);

        new_hankin.setParams(&params);

        poly_hankin.addHankin(new_hankin);
      }
      poly_hankins.push_back(poly_hankin);
    }
  }
}


//--------------------------------------------------------------
void ofApp::update(){
  sync.update();
  fps = ofGetFrameRate();
  ofBackground(0);

  for (int i =0; i<poly_hankins.size();i++){
    poly_hankins[i].update();
  }
  // view_x -= 0.5;
}

void ofApp::patternChanged(float & new_pattern_size){
  // createPattern();
}

void ofApp::modeChanged(bool & new_mode){
  // createPattern();
}

void ofApp::changed(float & x){
  for (int i =0; i<poly_hankins.size();i++){
    poly_hankins[i].updateHankins((x+i/100)*PI/180, delta);
  }
}

void ofApp::deltaChanged(float & delta){
  for (int i =0; i<poly_hankins.size();i++){
    poly_hankins[i].updateHankins((a+i/100)*PI/180, delta);
  }
}


//--------------------------------------------------------------
void ofApp::draw(){

  if (b_shader){
      shader.begin();
      shader.setUniform2f("size", ofGetWidth(), ofGetHeight());
    }

  ofBackground(0);

  if (do_svg_export){
    ofBeginSaveScreenAsSVG("star-"+ofGetTimestampString()+".svg");
  }

  ofSetLineWidth(line_width);
  ofPushMatrix();
  ofTranslate(view_x, ofGetHeight()/2);
  // ofScale(0.5,0.5);
  // ofRotate(ofGetElapsedTimef()*10);

  if (draw_pattern) {
      ofSetColor(100);
      for (int i =0; i<polys.size();i++){
        polys[i].draw();
      }
  }

  ofSetColor(255);
  if (draw_hankin){
    for (int i =0; i<poly_hankins.size();i++){
      poly_hankins[i].draw();
    }
  }

  float r = 500*2;
  if (draw_debug){
    ofSetColor(255, 0, 0);
    ofNoFill();
    for (int i =0; i<poly_centers.size();i++){
      ofDrawEllipse(poly_centers[i], 5, 5);
    }
    ofDrawEllipse(0, 0, r, r);
  }

  ofPopMatrix();

  if (b_shader) {
      shader.end();
    }

  if (do_svg_export){
    do_svg_export = false;
    ofEndSaveScreenAsSVG();
  }

  imgui.begin();
  ImGuiIO& imio = ImGui::GetIO();

  if (draw_gui){


    ImGui::Button("Draw Pattern");
    if (ImGui::IsItemClicked(0)){
      draw_pattern = !draw_pattern;
    }ImGui::SameLine();

    if(ImGui::Button("Draw Hankins")){
      draw_hankin = !draw_hankin;
    }

    // ImGUI::PushID("Draw Debug");

    if (draw_debug){
      ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.2f, 0.5f, 0.5f));
    }else{
      ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.2f, 0.0f, 0.5f));
    }


    ImGui::Button("Draw Debug");
    if(ImGui::IsItemClicked(0)){
      draw_debug = !draw_debug;
    }
    ImGui::PopStyleColor();

    // ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.5f, 0.0f, 0.5f));

    ImGui::SliderFloat("Line width", &line_width, 0.0f, 10.0f);

    bool update_pattern = ImGui::SliderFloat("Pattern size", &pattern_size, 10.0f, 500.0f);
    update_pattern = ImGui::SliderFloat("E", &size_alt_hex, 0.0f, 2.0f) || update_pattern;
    update_pattern = ImGui::SliderFloat("R", &ratio_hex, 0.0f, 2.0f) || update_pattern;

    ImGui::SliderFloat("Noise scale angle", &params.noise_scale_angle, 1.0f, 5000.0f);
    ImGui::SliderFloat("Noise scale delta", &params.noise_scale_delta, 1.0f, 5000.0f);
    ImGui::SliderFloat("Noise speed", &params.noise_speed, 0.0f, 2.0f);
    ImGui::SliderFloat("Noise pos", &params.noise_pos, 0.0f, 50.0f);

    static vector<string> vec;
    vec.clear();
    vec.push_back("Squares");
    vec.push_back("Hexas");
    vec.push_back("Hexas alt");
    vec.push_back("Octos");
    vec.push_back("Rhombi");


    bool update_mode = ofxImGui::VectorListBox("Mode", &current_mode, vec);
    if (update_mode){
      ofLog() << "Change mode: "  << vec[current_mode];
    }

    update_pattern = update_pattern || update_mode;

    if (update_pattern){
      createPattern();
    }

  }

  ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

  imgui.end();


  // if (draw_gui){
  //   gui.draw();
  // }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'g':
    draw_gui = !draw_gui;
    break;
  case 's':
    do_svg_export = true;
    break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::exit(){
  a.removeListener(this, &ofApp::changed);
}
