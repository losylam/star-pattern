#pragma once

#include "ofMain.h"
#include "hankin.h"


class PolyHankin {

 public:
  PolyHankin();
  void updateHankins(float angle, float delta);
  void update();
  void draw();

  void addHankin(Hankin & hankin);

  vector<Hankin> hankins;

  vector<ofPolyline> polys;
};
