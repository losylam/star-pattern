#pragma once

#include "ofMain.h"

#include "params.h"

class Hankin {

 public:
  Hankin(glm::vec3 a, glm::vec3 b, float poly_angle);
  void setParams(Params * params_in);
  void update();
  void update(float angle, float delta);
  void updateNoise();
  void updateSin();
  void draw();

  ofPoint a;
  ofPoint b;
  float poly_angle;
  Params *params;

  float seg_length;

  ofPoint h;
  ofPoint dir;
  ofPoint dir2;
  ofPoint h1;
  ofPoint h2;
  float length;
  float angle;
  float delta;
  float delta_px;
};
