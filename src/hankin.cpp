#include "hankin.h"

//--------------------------------------------------------------
Hankin::Hankin(glm::vec3 a_in, glm::vec3 b_in, float poly_angle_in){
  a = a_in;
  b = b_in;
  poly_angle = poly_angle_in;

  angle = PI/6;
  delta = 0;
  seg_length = glm::distance(a_in, b_in);
  delta_px = seg_length*delta/2;

  length = abs((seg_length/2 - delta_px) * sin(poly_angle/2.0)/sin(PI-poly_angle/2-angle));

  h = (b+a)/2;
  dir = (b-a).normalize();
  h2 = h + dir*delta_px;
  h1 = h - dir*delta_px;
  dir = dir * length;
  dir = dir.rotate(0, 0, -angle*180/PI);
  dir2= (b-a).normalize();
  dir2 = dir2 * length;
  dir2 = dir2.rotate(0, 0, 180+angle*180/PI);
}

void Hankin::setParams(Params * params_in){
  params = params_in;
}
void Hankin::update(){
  delta_px = seg_length*delta/2;

  length = abs((seg_length/2 - delta_px) * sin(poly_angle/2.0)/sin(PI-poly_angle/2-angle));
  if (length > 300){
    length = 300;
  }

  dir = (b-a).normalize();
  h2 = h + dir*delta_px;
  h1 = h - dir*delta_px;
  dir = dir * length;
  dir = dir.rotate(0, 0, -angle*180/PI);
  dir2= (b-a).normalize();
  dir2 = dir2 * length;
  dir2 = dir2.rotate(0, 0, 180+angle*180/PI);
}

void Hankin::update(float angle_in, float delta_in){
  angle = angle_in;
  if (angle > HALF_PI){
    angle = HALF_PI;
  }
  delta = delta_in;

  update();
}

void Hankin::updateNoise(){
  // float mod = (sin(ofGetElapsedTimef())+1)/2;
  float mod = 1;
  float n = ofNoise(h.x/params->noise_scale_angle,
                    h.y/params->noise_scale_angle,
                    params->noise_pos + ofGetElapsedTimef() * params->noise_speed);

  float n1 = ofNoise(h.x/params->noise_scale_delta,
                     h.y/params->noise_scale_delta,
                     params->noise_pos + 10+ofGetElapsedTimef() * params->noise_speed) * mod;
  // update(n*(HALF_PI + PI/6), (2*n1-1));
  update(n*(HALF_PI), ofMap(n1, 0, 1, 0.5, -1));
}

void Hankin::updateSin(){
  float d = h.distance(ofPoint(0,0,0));
  float a = h.angle(ofPoint(0,1,0));
  float n = sin(d/500 + ofGetElapsedTimef()*PI/4);
  delta = cos(ofGetElapsedTimef()*PI/6)/2 -0.5;
  // float n1 = ofNoise(h.x/2000, h.y/2000, 10+ofGetElapsedTimef()/2);
  // angle = ofMap(n, 1, -1, -3*PI/5, HALF_PI);
  angle = ofMap(n, 1, -1, -0, HALF_PI*2/3);
  update();
}

//--------------------------------------------------------------
void Hankin::draw(){


  // update(HALF_PI - n *(HALF_PI+PI/6), (2*n1-1));
  // ofDrawCircle(h, 3);
  ofDrawLine(h2, h2+dir);
  ofDrawLine(h1, h1+dir2);
}

