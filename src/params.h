#pragma once

struct Params {
  float noise_scale_angle;
  float noise_scale_delta;
  float noise_speed;

  float noise_pos;
};
