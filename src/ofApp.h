#pragma once

#include "ofMain.h"

#include "ofxImGui.h"
#include "ofxGui.h"
#include "ofxOscParameterSync.h"

#include "params.h"
#include "hankin.h"
#include "poly_hankin.h"

class ofApp : public ofBaseApp{

 public:
  void setup();
  void update();
  void draw();

  void exit();

  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);


  void changed(float & x);
  void deltaChanged(float & x);
  void patternChanged(float & x);
  void modeChanged(bool & x);


  ofPolyline polyReg(ofPoint center, float radius, int n, float start_angle);
  ofPolyline polyHex(ofPoint center, float radius, float e);

  void createSquare();
  void createHex();
  void createHexSq();
  void createOcto();
  void createRhombi();
  void createRhombiCell(float pos_x, float pos_y);

  void createPattern();

  void updateHankins();

  bool b_shader;

  // GUI

  ofxOscParameterSync sync;

  ofxPanel gui;

  ofxImGui::Gui imgui;
  float line_width;
  float pattern_size;
  float size_alt_hex;
  float ratio_hex;
  int current_mode;

  float noise_scale;

  float view_x;

  Params params;
  ofParameter<bool> draw_gui;
  ofParameter<bool> draw_debug;
  ofParameter<bool> draw_pattern;
  ofParameter<bool> draw_hankin;
  /* ofParameter<float> pattern_size; */
  ofParameter<float> a;
  ofParameter<float> delta;

  ofParameterGroup group_mode;

  ofParameter<bool> mode_squares;
  ofParameter<bool> mode_hexa;
  ofParameter<bool> mode_octo;

  ofParameterGroup group;

  ofParameter<bool> mode_manual;
  ofParameter<bool> mode_noise;
  ofParameter<bool> mode_sine;

  ofxIntSlider fps;

  // export
  bool do_svg_export;

  // process
  vector<Hankin> hankins;
  vector<PolyHankin> poly_hankins;

  ofPolyline poly;
  vector<ofPolyline> polys;
  vector<ofPoint> poly_centers;

  ofShader shader;
};

