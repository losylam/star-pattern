#include "poly_hankin.h"

//--------------------------------------------------------------
PolyHankin::PolyHankin(){
}

//--------------------------------------------------------------
void PolyHankin::updateHankins(float angle, float delta){
  for (int i =0; i<hankins.size();i++){
    Hankin & h1 = hankins[i];
    h1.update(angle, delta);
  }

}

//--------------------------------------------------------------
void PolyHankin::update(){

  for (int i =0; i<hankins.size();i++){
    Hankin & h1 = hankins[i];
    h1.updateNoise();
    // h1.updateSin();
  }

  polys.clear();

  for (int i =0; i<hankins.size();i++){
    Hankin & h1 = hankins[i];
    Hankin & h2 = hankins[(i+1)%hankins.size()];

    ofPolyline poly1;
    poly1.addVertex(h1.h2);

    ofPoint pt1 = (h1.h2 + h1.dir + h2.h1 + h2.dir2) * 0.5;
    poly1.addVertex(pt1);
    poly1.addVertex(h2.h1);

    polys.push_back(poly1);
  }
}

//--------------------------------------------------------------
void PolyHankin::draw(){
  // for (int i =0; i<hankins.size();i++){
  //   Hankin h1 = hankins[i];
  //   ofSetColor(255, 0, 0);
  //   h1.draw();
  // }

  ofSetColor(255);
  for (int i =0; i<polys.size();i++){
    polys[i].draw();
  }
}

//--------------------------------------------------------------
void PolyHankin::addHankin(Hankin & hankin){
  hankins.push_back(hankin);
}

