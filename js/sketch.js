import patterns from './patterns.js';

let size_init = 100;
let step_x, step_y;
let step_x_j, step_y_i;
let h_hexa = Math.sqrt(3)/2;
let pattern_pts;
let polys;
let polys_pts;
let polys_all = [];
let pts = [];

let start_pause = false;

let n = 20;

let posh=0;
let posa = 0;

let do_export = false;
let is_exporting = false;

const gui = new dat.GUI();
// gui.close();

let r_debug = 13;
const params = {
    pattern: 'hexa',
    size: size_init,
    angle: 0,
    pause: false,
    draw_pattern: false,
    draw_hankins: true,
    draw_poly: false,
    debug: false,
    projection: false,
    dispersion: false,
    opacity: 0,
    stroke_weight: 1,
    anim_loop: false,
    blend: false,
    a: Math.PI/3,
    // a: 0,
    h: 0,
    anim_a:0,
    anim_h:2,
    mina: 0.0,
    maxa: 0.5,
    minh: -0.5,
    maxh: 0.0,
    noise: {
        scale_a: 100,
        scale_h: 2000,
        pos_a: 2,
        pos_h: 2,
    },
    mod_a: 'uniform',
    mod_h: 'uniform',
    pos: 1,
    range: {
        active: true,
        circle: false,
        width: 0.6,
        height: 0.8,
    },

    save:()=> {
        do_export = true;
    },
};

const mod_types = [
    'uniform',
    'linear',
    'noise',
    'wave',
    'sine'
];

const gui_pattern = gui.addFolder('pattern');
gui_pattern.open();
gui_pattern.add(params, 'pattern', Object.keys(patterns)).listen().onChange(()=>compose_pattern());
gui_pattern.add(params, 'size', 10, 200, 0.1).listen().onChange(()=>compose_pattern());
gui_pattern.add(params, 'a', -2, 2, 0.01).listen();
gui_pattern.add(params, 'h', -2, 2, 0.01).listen();

const gui_d = gui.addFolder('drawing');
// gui_d.open();
gui_d.add(params, 'draw_pattern').listen();
gui_d.add(params, 'draw_hankins').listen();
gui_d.add(params, 'debug').listen();
gui_d.add(params, 'blend');


gui_d.add(params, 'opacity', 0, 255, 1);
gui_d.add(params, 'stroke_weight', 0, 10, 0.1);

let gui_edge = gui.addFolder('edge');
gui_edge.add(params.range, 'active');
gui_edge.add(params.range, 'circle');
gui_edge.add(params.range, 'width', 0, 1, 0.01);
gui_edge.add(params.range, 'height', 0, 1, 0.01);

const gui_anim = gui.addFolder('animation');
gui_anim.add(params, 'mod_a', mod_types);
gui_anim.add(params, 'mod_h', mod_types);
gui_anim.add(params, 'pause').listen();
// gui_anim.add(params, 'anim_loop');
gui_anim.add(params, 'anim_a', 0, 2, 0.01).listen();
gui_anim.add(params, 'anim_h', 0, 2, 0.01).listen();

gui_anim.add(params, 'mina', -2, 2, 0.01);
gui_anim.add(params, 'maxa', -2, 2, 0.01);
gui_anim.add(params, 'minh', -1, 1, 0.01);
gui_anim.add(params, 'maxh', -1, 1, 0.01);
gui_anim.add(params, 'pos', 0, 2, 0.01).onChange(() => compose_pattern()).listen();


let gui_noise = gui.addFolder('noise');
gui_noise.add(params.noise, 'scale_a', 0, 1000, 1);
gui_noise.add(params.noise, 'pos_a', 0, 10, 0.01);
gui_noise.add(params.noise, 'scale_h', 0, 1000, 1);
gui_noise.add(params.noise, 'pos_h', 0, 10, 0.01);


gui.add(params, 'save').name('save [s]');


let n_max = 0;
let n_min = 1;



let current_pattern = patterns.truncated_square;


let states = [
    {
        pos: 0.9,
        a: 0.7,
        h: 0.0,
        size: 80,
    },
    {
        pos: 0,
        a: 0.3,
        h: -0.5,
        size: 80
    },
    {
        pos: 0,
        a: -0,
        h: 0.0,
        size: 80
    },
    {
        pos: 0,
        a: 0.5,
        h: 0.5,
        size: 80
    },
    {
        pos: 0,
        a: 1,
        h: 0.0,
        size: 80
    },
];

states = [
    {
        mina: 1,
        maxa: 0.95,
        minh: 0,
        maxh: 0,
        pos:7,
        size: 70
    },
    {
        mina: -1.5,
        maxa: -0.5,
        minh: -2,
        maxh: 0,
        pos: 1.2,
        size: 70
    },
    {
        mina: 1,
        maxa: 0.9,
        minh: -2,
        maxh: 0,
        pos: 0,
        size: 80
    },
    {
        mina: 1,
        maxa: -1,
        minh: 0,
        maxh: -1,
        pos: -1,
        size: 80
    },
    {
        mina: 0.9,
        maxa: 0.6,
        minh: 0,
        maxh: 0,
        pos: -1,
        size: 120
    },
    {
        mina: 0.0,
        maxa: 0.9,
        minh: -0.9,
        maxh: 0,
        pos: 0,
        size: 90
    },
];

states = [
    {
        pos: 0,
        size: 100,
        // angle: 3*Math.PI/2,
    },
    {
        pos: 7,
        size: 100,
        // angle: Math.PI/2,
    },
    {
        pos: 1,
        size: 100,
        // angle: Math.PI,
    },
    {
        pos: -5,
        size: 100,
        // angle: Math.PI,
    },
];

let states_mult = [];
for (let i =0; i <10; i++){
    for (let j=0; j<states.length; j++){
        states_mult.push({
            pos: states[j].pos,
            size: states[j].size,
            angle: ( i*4 + j + 1 ) * Math.PI/4,
        });
    }
}

let anim = new AnimParam(2000, 1.1);
anim.states = states_mult;

function setup(){
    let canvas = createCanvas(windowWidth, windowHeight);
    compose_pattern();
    canvas.elt.addEventListener('wheel', event=>console.log('click'));
    anim.next();
}


function draw(){
    if (params.pause) return;
    strokeCap(ROUND);
    strokeJoin(ROUND);

    if (params.anim_loop){
        if (anim.start){
            anim.step();

            for (let [key, value] of Object.entries(anim.param)){
                if ( params[key] != undefined ){
                    console.log(key);
                    params[key] = value;
                }
            }
            // params.pos = anim.param.pos;
            // params.a = anim.param.a;
            // params.h = anim.param.h;
            // params.size = anim.param.size;
            compose_pattern();
        }else{
            anim.next();
        }
    }

    if (do_export && !is_exporting){
        is_exporting = true;
        console.log('save');
        save_svg();
    }else if(!do_export && !is_exporting){
        blendMode(BLEND);
        background(0);
        translate(width/2, height/2);

        // fill(255);
        // ellipse(0, 0, 700);
        rotate(params.angle);

        posh = map(mouseX, 0, width, -1, 1);
        posa = map(mouseY, 0, height, -1, 1);
        // posa = 0;

        noFill();
        // for (let j=-4; j<4;j++){
        //     let y = step_y*j;
        //     for (let i=-4; i<4; i++){
        //         let x = step_x*i;
        //         if (abs(j)%2==1){
        //             x += step_x/2;
        //         }
        //         draw_pattern(x, y);
        //     }
        // }

        if (params.blend){
            blendMode(DIFFERENCE);
        }
        draw_pattern();
    }

    if (params.debug){
        draw_debug();
    }
    if (start_pause) {
        start_pause = false;
        params.pause = true;
    };
    // stroke('red');
    // rect(-height/2, -height/2, height, height);
}

function draw_debug(){
    noStroke();
    fill(0, 220, 0);
    for (let i_pt =0; i_pt<pattern_pts.length;i_pt++){
        ellipse(pattern_pts[i_pt][0], pattern_pts[i_pt][1], r_debug);
        text(i_pt, pattern_pts[i_pt][0]+10, pattern_pts[i_pt][1]-5);
    }
    
    if (params.range.active){
        stroke('purple');
        noFill();
        if (params.range.circle){
            ellipse(0, 0, params.range.width*height*2);
        }else{
            rect(-params.range.width*width,
                 -params.range.height*height,
                 params.range.width*width*2,
                 params.range.height*height*2,
                );
        }
    }
}

function get_pattern_pts(){
    let s = params.size;
    let h = h_hexa*s;
    [step_x, step_y] = current_pattern.steps(params);

    pattern_pts = current_pattern.pts(params);
    polys = current_pattern.polys();
}

function compose_pattern(){
    current_pattern = patterns[params.pattern];
    pts = [];
    get_pattern_pts();
    polys_pts = [];
    let polys_grid = {};
    let cpt = 0;
    for (let j=-n; j<=n;j++){
        polys_grid[j] = {};
        let y = step_y*j;
        let x0 = 0;
        if ( step_x_j ) x0 = step_x_j * j ;
        for (let i=-n-1; i<=n; i++){

            let x = x0 + step_x*i;
            if (current_pattern.offset){
                if (abs(j)%2==1){
                    x += step_x/2;
                }
            }

            if (step_y_i) y = step_y*j + i*step_y_i;

            let poly = {
                i_pt0: cpt,
                i: i,
                j: j,
                x: x,
                y: y,
                pts: [],
                indices: [],
                hankins: []
            };

            poly.in_range = in_window(poly);

            polys_grid[j][i] = poly;
            polys_pts.push(poly);
            let poly_pts = get_translated(pattern_pts, x, y);
            let new_points = poly_pts.map(pt => {
                cpt += 1;
                return {
                    x: pt[0],
                    y: pt[1],
                    z: 0,
                    i: cpt-1
                };
            });
            pts = pts.concat(new_points);
        }
    }

    for (let j=-n+1; j<=n-1; j++){
        for (let i=-n+1; i<=n-1; i++){
            let lst_poly = [];
            let cur_poly = polys_grid[j][i];
            for (const poly of polys){
                lst_poly.push(poly.map(i=>i+cur_poly.i_pt0));
            }
            cur_poly.indices = lst_poly;
        }
    }

    console.log('poly pts', polys_pts);
    polys_pts.filter(p=>p.in_range).forEach(poly=>{
        let xp = polys_grid[poly.j][poly.i+1].i_pt0;
        let xm = polys_grid[poly.j][poly.i-1].i_pt0;
        let yp = polys_grid[poly.j+1][poly.i].i_pt0;
        let ym = polys_grid[poly.j+1][poly.i-1].i_pt0;
        if (current_pattern.offset && Math.abs(poly.j)%2 == 1){
            yp = polys_grid[poly.j+1][poly.i+1].i_pt0;
            ym = polys_grid[poly.j+1][poly.i].i_pt0;
        }
        let i0 = poly.i_pt0;

        poly.indices = [...poly.indices, ...current_pattern.indices(i0, xm, xp, ym, yp)];
        poly.hankins = current_pattern.hankins(i0, xm, xp, ym, yp);
    });

    get_polys_all();
}

function get_polys_all(){
    polys_all = [];
    polys_pts.forEach(poly=>{
        poly.indices.forEach(sub_poly=>{
            let x = 0;
            let y = 0;
            let poly_pts = [];
            sub_poly.forEach(i_pt => {
                let pt = pts[i_pt];
                x += pt.x;
                y += pt.y;
                poly_pts.push(pt);
            });

            x = x/(sub_poly.length);
            y = y/(sub_poly.length);

            let pts_tr = poly_pts.map(pt => {
                return {
                    x: pt.x-x,
                    y: pt.y-y,
                };
            });


            let new_poly = {
                center: {
                    x: x,
                    y: y
                },
                pts: poly_pts,
                pts_tr: pts_tr,
            };
            polys_all.push(new_poly);
        });
    });
}


let bw = true;
let anim_pos;
let base_duration = 800;
let duration = base_duration;
let last = new Date();
let anim_cpt = 0;
let n_rot = 1;

let anim_e = easeInOut(2);

let dec = 0.3;
function get_anim_d(d){
    let start_pos = 0;
    if (d>0.0){
        start_pos = map(d, 0, 1, 0, 0.7);
    }
    let end_pos = map(d, 0, 1, 1, 1);
    let pos = map(anim_pos, start_pos, end_pos, 0, 1);
    return anim_e(constrain(pos, 0, 1));
}

let tr_scale = 0.2;
let rect_scale = 0.13;

function draw_poly(){
    strokeWeight(2);
    let now = new Date();
    if (now-last < duration){
        anim_pos = (now-last)/duration;
    }else{
        anim_pos = 0;
        last = now;
        anim_cpt += 1;
        if ( anim_cpt%2 == 0 ){
            tr_scale = random([1.45, 0.2, 0.355]);
            rect_scale = random([0.13, 0.95, 0.6, 2]);
            // rect_scale = ;
            n_rot = random([1, 1, 2, 3, 3, 4]);
            // n_rot = 2;
            if(n_rot>1){
                duration = n_rot*base_duration/2;
            }else{
                duration = 1*base_duration;
            }
            if (duration == base_duration && tr_scale > 1 || rect_scale >1){
                duration = 1.5*duration;
            }
        }
    }
    // anim_pos = ( 1 + Math.sin(millis()/500) )/2;
    if ((anim_cpt)%4<2){
        draw_poly_tr();
    }else{
        draw_poly_rect();
    }
}

function draw_poly_simple(){
    polys_all.forEach(poly =>{
        // stroke('white');
        push();
        translate(poly.center.x, poly.center.y);

        if (poly.pts_tr.length==4){
            stroke('white');
            fill('white');
        }else{
            stroke('white');
            fill('black');
        }
        beginShape();
        poly.pts_tr.forEach(pt=>{
            vertex(pt.x, pt.y);
        });
        endShape(CLOSE);

        pop();
    });
}


function draw_poly_tr(){
    // console.log(polys_all);
    if (bw) {
        if (params.blend){
            blendMode(BLEND);
            background('white');

            blendMode(DIFFERENCE);
            fill('white');
        }else{
            background('white');
            noStroke();
            fill('black');
        }
    }else{
        background('black');
        noStroke();
        // fill('black');
        stroke('white');
    }
    let i_poly = 0;
    polys_all.forEach(poly =>{
        // stroke('white');
        push();
        translate(poly.center.x, poly.center.y);
        let s = millis()/1000;
        s = 1;
        let d = dist(0, 0, poly.center.x, poly.center.y)/(height/1.2);
        // d = ( poly.center.x + width/2 )/width;
        let pos = get_anim_d(d);
        // let pos = anim_e(anim_pos);
        let pos_start = 0;
        if (anim_cpt%2 == 1){
            pos_start = 1;
        }
        let dir = 1;
        if (anim_cpt%8<4){
            dir = -1;
        }
        let pos_scale = 1+pos*tr_scale;
        if (anim_cpt%2 == 1){
            pos_start = 1;
            pos_scale = (1+tr_scale -pos*tr_scale);

        }
        if (poly.pts_tr.length != 4){
            if (i_poly%2 == 0){
                dir = -dir;
            }
            if (poly.pts_tr.length == 3){
                rotate(n_rot*dir*( pos_start + pos ) * Math.PI/3);
            }else{
                rotate(n_rot*dir*(pos_start + pos) * Math.PI/6);

            }
            scale(pos_scale);
            strokeWeight(2.5/pos_scale);
            beginShape();
            poly.pts_tr.forEach(pt=>{
                vertex(pt.x, pt.y);
            });
            endShape(CLOSE);
            i_poly +=1;
        }
        pop();
    });
}

function draw_poly_rect(){
    // console.log(polys_all);
    if (bw) {
        background('black');
        noStroke();
        fill('white');
    }else{
        background('black');
        // fill('black');
        stroke('white');
    }
    let i_poly = 0;
    polys_all.forEach(poly =>{
        push();
        translate(poly.center.x, poly.center.y);
        // s = 1;
        let d = dist(0, 0, poly.center.x, poly.center.y)/(height/1.2);
        // d = ( poly.center.x + width/2 )/width;
        let pos = get_anim_d(d);
        let pos_start = 0;
        let pos_scale = (1 + pos*rect_scale);
        if (anim_cpt%2 == 1){
            pos_start = 1;
            pos_scale = (1+rect_scale -pos*rect_scale);

        }
        let dir = 1;
        if (poly.pts_tr.length == 4){
            if (anim_cpt%8<4){
                dir = -1;
            }
            scale(pos_scale);
            strokeWeight(2.5/pos_scale);
            rotate(n_rot * dir * ( pos_start + pos ) * Math.PI/4);
            beginShape();
            poly.pts_tr.forEach(pt=>{
                vertex(pt.x, pt.y);
            });
            endShape(CLOSE);
            i_poly += 1;
        }
        pop();
    });
}

function draw_pattern(){
    // noStroke();
    if (params.draw_pattern){
        stroke(200, 50, 50);
        strokeWeight(1);
        // stroke('red');
        for (const poly of polys_pts){
            // fill('red');
            // text(poly.i, poly.x, poly.y);
            noFill();
            poly.p_coords = [];
            for (let i_pts of poly.indices){
                let p_coords = [];
                // fill('red');
                // fill(random(['red', 'blue', 'green']));
                // if (i_pts.length == 5){
                    // fill('black');
                // }else if(i_pts.length == 4){
                //     fill('white');
                // }
                beginShape();
                for (let i_pt of i_pts){
                    let x = pts[i_pt].x;
                    let y = pts[i_pt].y;
                    if (params.projection){
                        [x, y] = cart_to_hyper(pts[i_pt].x, pts[i_pt].y, 600);
                    }
                    // vertex(pts[i_pt].x, pts[i_pt].y);
                    vertex(x, y);
                    p_coords.push([pts[i_pt].x, pts[i_pt].y]);
                }
                poly.p_coords.push(p_coords);
                endShape();
            }
        }
    }

    if (params.draw_hankins){
        noFill();
        stroke('white');
        fill(255, 255, 255, params.opacity);
        strokeWeight(params.stroke_weight);
        draw_hankins();
    }
    if (params.draw_poly){
        // draw_poly();
        draw_poly_simple();
    }
    // stroke('purple');
    // noLoop();
}


let last_update;
let time_a = 0;
let time_h = 0;
function draw_hankins(){
    let now = new Date();
    if (last_update){
        let dur = ( now-last_update )/1000;
        time_a += params.anim_a*dur;
        time_h += params.anim_h*dur;
    }
    last_update = now;

    for (const poly of polys_pts){
        for (let hankin of poly.hankins){
            draw_hankin(hankin);
        }
    }

}


function is_in_range(pt){
    let dd = dist(pt.x, pt.y, 0, 0);
    if (params.range.active){
        if (params.range.circle){
            if (dd > height*params.range.width) return false;
        }else{
            if (Math.abs(pt.x) > width * params.range.width) return false;
            if (Math.abs(pt.y) > height * params.range.height) return false ;
        }
    }
    return true;
}

function in_window(pt){
    return (pt.x > -width/2 - params.size * 5)
        && (pt.x < width/2 + params.size * 5)
        && (pt.y > -height/2 - params.size * 5)
        && (pt.y < height/2 + params.size*5);

}

function draw_hankin(hankin){
    // fill(0, 200);
    beginShape();
    // noStroke();
    let pt0 = pts[hankin.i];
    let dd = dist(pt0.x, pt0.y, 0, 0);
    if (!is_in_range(pt0)) return;

    if (params.debug){
        fill('red');
        noStroke();
        ellipse(pt0.x, pt0.y, r_debug);
        fill(255, 255, 255, params.opacity);
        stroke(255);
    }

    let posa = 0;
    let posh = 0;

    if (params.mod_h == 'uniform'){
        posh = params.h;
    }
    if (params.mod_a == 'uniform'){
        posa = params.a;
    }

    if (params.mod_h == 'noise'){
        posh = noise(4000 + params.noise.pos_h + pt0.y/params.noise.scale_h, 4000 + pt0.x/params.noise.scale_h+time_h);
        if (posh>n_max){
            n_max = posh;
        }
        if (posh<n_min){
            n_min = posh;
        }
        posh = map(posh, 0.3, 0.7, params.minh, params.maxh, true);
    }

    if (params.mod_a == 'noise'){
        posa = noise(3000 + params.noise.pos_a + pt0.y/params.noise.scale_a, 4000 + pt0.x/params.noise.scale_a + time_a);
        posa = map(posa, 0.3, 0.7, params.mina, params.maxa, true);
    }


    if (params.mod_a == 'linear'){
        posa = map(pt0.x, -width/2, width/2, params.mina, params.maxa);
    }
    if (params.mod_h == 'linear'){
        posh = map(pt0.y, -height/2, height/2, params.minh, params.maxh);
    }

    if (params.mod_a == 'wave'){
        let posa_sin = Math.sin(map(pt0.y, -height/2, height/2, 0, PI*2)+time_a);
        posa = map(posa_sin, -1, 1, params.mina, params.maxa);
    }
    if (params.mod_h == 'wave'){
        let posh_sin = Math.sin(map(pt0.x, -height/2, height/2, 0, PI*2)+time_h);
        posh = map(posh_sin, -1, 1, params.minh, params.maxh);
    }


    if (params.mod_a == 'sine'){
        let posa_sin = Math.sin(map(dd, 0, height, 0, PI*2)-time_a);
        posa = map(posa_sin, -1, 1, params.mina, params.maxa);
    }

    if (params.mod_h == 'sine'){
        let posh_sin = Math.sin(map(dd, 0, height, 0, PI*2)-time_h);
        posh = map(posh_sin, -1, 1, params.minh, params.maxh);
    }


    hankin.coords = [];
    let rot = 0;
    let max_rot = 3.2;
    let max_tr = 550;

    let tr = [];
    if (params.dispersion){
        let amp_rot = pow(map(pt0.x, -width/2, width/2, 1, 0), 2.5);
        let n_rot = noise(3000 + params.noise.pos_a + pt0.y/params.noise.scale_a, 4000 + pt0.x/params.noise.scale_a + time_a);
        rot = amp_rot*map(n_rot, 0.3, 0.7, -max_rot, max_rot);
        let t_a= noise(1000 + params.noise.pos_a + pt0.y/params.noise.scale_a, 1000 + pt0.x/params.noise.scale_a + time_a);
        let t_v = noise(2000 + params.noise.pos_a + pt0.y/params.noise.scale_a, 2000 + pt0.x/params.noise.scale_a + time_a);

        t_a = amp_rot*map(t_a, 0.3, 0.7, 0, TWO_PI);
        t_v = amp_rot*map(t_v, 0.3, 0.7, -max_tr, max_tr);
        tr[0] = t_v*cos(t_a);
        tr[1] = t_v*sin(t_a);
        tr[1] *= 3;
    }

    hankin.pts.forEach((i_pt, i) =>{
        // far vertex of edges
        let pt1 = pts[i_pt];
        let pt2 = pts[hankin.pts[( i+1 )%hankin.pts.length]];

        // // angle between edges
        // let pt1_a = createVector(pt1.x - pt0.x, pt1.y - pt0.y);
        // let pt2_a = createVector(pt2.x - pt0.x, pt2.y - pt0.y);

        // let a = pt1_a.angleBetween(pt2_a)/2;

        // // position of the hankin
        // let h = dist(pt0.x, pt0.y, pt1.x, pt1.y);
        // h = (1+posh) * h / 2;

        // // root of the hankin (p1)
        // let dir = createVector(pt1.x-pt0.x, pt1.y-pt0.y);
        // dir.setMag(h);

        // let p1 = [pt0.x + dir.x, pt0.y + dir.y];

        // // head of the hankin (p2)
        // let l = h * Math.sin(a) / Math.sin(Math.PI/2 + posa - a);

        // dir.rotate(PI/2 + posa);
        // dir.setMag(l);

        // let p2 = [p1[0]+dir.x, p1[1]+dir.y];
        const pt0_v = createVector(pt0.x, pt0.y, 0);
        const pt1_v = createVector(pt1.x, pt1.y, 0);
        const pt2_v = createVector(pt2.x, pt2.y, 0);
        let [p1, p2] = get_hankins(pt0_v, pt1_v, pt2_v, posh, posa);

        // let [x1, y1] = p1;
        // let [x2, y2] = p2;

        let [x1, y1] = [p1.x, p1.y];
        let [x2, y2] = [p2.x, p2.y];

        // transformation if any
        if (params.projection){
            [x1, y1] = cart_to_hyper(p1[0], p1[1], 300);
            [x2, y2] = cart_to_hyper(p2[0], p2[1],  300);
        }
        if (params.dispersion){
            [x1, y1] = rot_around(x1, y1, pt0, rot);
            [x2, y2] = rot_around(x2, y2, pt0, rot);
            [x1, y1] = [x1 + tr[0], y1 + tr[1]];
            [x2, y2] = [x2 + tr[0], y2 + tr[1]];
        }

        // draw
        if (params.debug){
            noStroke();
            fill('yellow');
            ellipse(p1.x, p1.y, r_debug);
            noFill();
            stroke('white');
        }
        vertex(x1, y1);
        vertex(x2, y2);

        // save
        hankin.coords.push([x1, y1]);
        hankin.coords.push([x2, y2]);


    });
    endShape(CLOSE);
}

function get_hankins(pt0, pt1, pt2, posh, posa){
    // angle between edges
    // let pt1_a = createVector(pt1.x - pt0.x, pt1.y - pt0.y);
    let pt1_a = p5.Vector.sub(pt1, pt0);
    let pt2_a = p5.Vector.sub(pt2, pt0);

    let a = pt1_a.angleBetween(pt2_a)/2;

    // position of the hankin
    let h = pt0.dist(pt1);
    h = h * (1+posh) / 2;

    // root of the hankin (p1)
    let dir = p5.Vector.sub(pt1, pt0);
    dir.setMag(h);

    // let p1 = [pt0.x + dir.x, pt0.y + dir.y];
    let p1 = p5.Vector.add(pt0, dir);

    // head of the hankin (p2)
    let l = h * Math.sin(a) / Math.sin(Math.PI/2 + posa - a);

    dir.rotate(PI/2 + posa);
    dir.setMag(l);

    // let p2 = [p1[0]+dir.x, p1[1]+dir.y];
    let p2 = p5.Vector.add(p1, dir);

    return [p1, p2];
}

function get_translated(pattern_pts, x, y){
    let out_pts = [];
    for (let i=0; i<pattern_pts.length; i++){
        let xx = pattern_pts[i][0] + x;
        let yy = pattern_pts[i][1] + y;
        out_pts.push([xx, yy]);
    }
    return out_pts;
}

// function draw_pattern(x, y){
//     push();
//     translate(x, y);
//     for (const poly of polys){
//         beginShape();
//         for (const i_pt of poly){
//             vertex(pattern_pts[i_pt][0], pattern_pts[i_pt][1]);
//         }
//         endShape(CLOSE);
//     }
//     pop();
// }

function mouseWheel(event){
    console.log(event);
    if (event.delta>0){
        params.size = params.size*0.8;
    }else{
        params.size = params.size/0.8;
    }
    compose_pattern();
}


function windowResized(){
    console.log('resize');
    resizeCanvas(windowWidth, windowHeight);
}

function poly_to_d(pts){
    const p = document.createElementNS(svg_ns, 'path');
    let d = `M ${pts[0][0]} ${pts[0][1]} `;
    for (const pt of pts){
        d += `L ${pt[0]} ${pt[1]} `;
    }
    d += 'Z';
    p.setAttribute('d', d);
    return p;
}

function save_svg(){
    const gp = document.createElementNS(svg_ns, 'g');
    gp.setAttribute('id', 'layer1');
    gp.setAttribute('inkscape:groupmode', 'layer');
    gp.setAttribute('inkscape:label', 'pattern');

    const gh = document.createElementNS(svg_ns, 'g');
    gh.setAttribute('id', 'layer2');
    gp.setAttribute('inkscape:groupmode', 'layer');
    gh.setAttribute('inkscape:label', 'hankins');


    for (const poly of polys_pts ){
        if (poly.hankins){
            poly.hankins.forEach(h => {
                if (is_in_range(pts[h.i])) {
                    let pts = h.coords;
                    let p = poly_to_d(pts);
                    p.setAttribute('style', 'fill:none;stroke:#000');
                    gh.appendChild(p);
                }
            });
        }
        if (poly.p_coords && poly.p_coords.length>0){
            if (is_in_range(poly)) {
                poly.p_coords.forEach(pts=>{
                    let pp = poly_to_d(pts);
                    pp.setAttribute('style', 'fill:none;stroke:#555');
                    gp.appendChild(pp);
                });
            }
        }
    }

    setTimeout(()=>{
        is_exporting = false;
    }, 2000);

    do_export = false;

    export_svg([gp, gh]);
}

function export_svg(svg_content){
    const svg_ns = 'http://www.w3.org/2000/svg';
    const svg = document.createElementNS(svg_ns, 'svg');
    svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    svg.setAttribute("width", "1000");
    svg.setAttribute("height", "1000");


    if (svg_content){
        for (const cont of svg_content){
            svg.appendChild(cont);
        }
    }

    // // without d3
    // let path_d = `M${pts[0][0]}, ${pts[0][1]} `;
    // for (let i=1; i<pts.length; i++){
    //     path_d += `L${pts[i][0]}, ${pts[i][1]}`;
    // }

    var svg_data = svg.outerHTML;
    var preface = '<?xml version="1.0" standalone="no"?>\r\n';
    var svg_blob = new Blob([preface, svg_data], {type:"image/svg+xml;charset=utf-8"});
    var svg_url = URL.createObjectURL(svg_blob);

    var link = document.createElement("a");
    link.setAttribute("href", svg_url);
    link.setAttribute("download", `start_pattern_web.svg`);
    document.body.appendChild(link); // Required for FF
    link.click();
    document.body.removeChild(link); // Required for FF
}

function keyTyped(){
    if (key==='s'){
        do_export = true;
    }else if(key == 'd'){
        params.debug = !params.debug;
    }else if(key == 'H'){
        params.draw_hankins = !params.draw_hankins;
    }else if(key == 'p'){
        params.draw_pattern = !params.draw_pattern;
    }else if(key == 'n'){
        let lst_patterns = Object.keys(patterns);
        let pat = lst_patterns.indexOf(params.pattern);
        pat = (pat+1)%(lst_patterns.length);
        params.pattern = lst_patterns[pat];
        compose_pattern();
    }else if(key == 'N'){
        let lst_patterns = Object.keys(patterns);
        let pat = lst_patterns.indexOf(params.pattern);
        pat = pat-1;
        if (pat<0) pat = lst_patterns.length-1;
        params.pattern = lst_patterns[pat];
        compose_pattern();
    }
}

window.setup = setup;
window.draw = draw;
window.keyTyped = keyTyped;
window.windowResized = windowResized;
