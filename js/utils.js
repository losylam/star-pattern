const lerp = (x, y, a) => x * (1 - a) + y * a;
const clamp = (a, min = 0, max = 1) => Math.min(max, Math.max(min, a));
const invlerp = (x, y, a) => clamp((a - x) / (y - x));
const range = (x1, y1, x2, y2, a) => lerp(x2, y2, invlerp(x1, y1, a));

const dist = (x1, y1, x2, y2) => Math.hypot(x2-x1, y2-y1);

const easeIn  = p => t => Math.pow(t, p);
const easeOut = p => t => (1 - Math.pow(Math.abs(t-1), p));
const easeInOut = p => t => t<.5 ? easeIn(p)(t*2)/2 : easeOut(p)(t*2 - 1)/2+0.5;

// p5.Vector utils

const sub = (v1, v2) => p5.Vector.sub(v1, v2);
const add = (v1, v2) => p5.Vector.add(v1, v2);
const vtx = (pt) => vertex(pt.x, pt.y);

const do_for = (n, f) => {
    for (let i = 0; i<n; i++){
        f(i);
    }
};

function rot_around(x, y, pt, a){
    let dx = x-pt.x;
    let dy = y-pt.y;
    let rotx = dx*cos(a) - dy*sin(a);
    let roty = dx*sin(a) + dy*cos(a);

    return [pt.x + rotx, pt.y + roty];
}

function cart_to_hyper(x, y, r, k){
    x = x + 200*Math.cos(millis()/4000);
    y = y + 200*Math.sin(millis()/4000);

    let a2 = millis()/2500;

    let x1 = x*cos(a2)-y*sin(a2);
    let y1 = x*sin(a2)+y*cos(a2);

    x = x1;
    y = y1;

    let d = Math.hypot(x, y);
    let a = Math.atan2(x, y);

    r = 350;

    
    // if (d<r/10) return [x, y];


    // let dh = r + r * (2-(2*pow(d/r, -4)))/3;

    // let dh = r * (1- pow(r * pow(d+r, -1), 12));

    // return [dh*sin(a), dh*cos(a)];

    let x2 = abs(x);
    let y2 = abs(y);

    let xx = r*(1- pow(r/(x2+r), 6));
    let yy = r*(1- pow(r/(y2+r), 6));
    
    if (x <0 && y>0) {
        return [-xx, yy];
    }else if(x>0 && y<0){
        return [xx, -yy];
    }else if(x<0 && y<0){
        return [-xx, -yy];
    }
    return [xx, yy];
    // return [xx*sin(a), yy*cos(a)];
}

function get_anims(param1, param2){
    let to_anim = [];
    for (const [key, value1] of Object.entries(param1)){
        if (typeof value1 == "number"){
            const value2 = param2[key];
            if (value1 != value2){
                to_anim.push({
                    key: key,
                    start: value1,
                    end: value2
                });
            }
        }
    }
    return to_anim;
}


class AnimParam{
    constructor(duration, pow=2){
        this.duration = duration;
        this.start = undefined;
        this.inter = undefined;
        this.to_anim = [];
        this.param = undefined;
        this.e = easeInOut(pow);
        this.states = [];
        this.state = 0;
    }

    transition(param1, param2){
        this.param = Object.assign({}, param1);
        this.param.id = param2.id;
        this.start = new Date();

        this.to_anim = get_anims(param1, param2);
        this.next_id = param2.id;
    }

    step(){
        const now = new Date();
        let dur = now - this.start;
        if (dur>=this.duration){
            dur = this.duration;
            this.start = undefined;
        }
        const pos = this.e(dur/this.duration);
        for (const elem of this.to_anim){
            let val = lerp(elem.start, elem.end, pos);
            this.param[elem.key] = val;
        }
        if (this.start){
            return true;
        }else{
            return false;
        }
    }

    next(){
        let p = this.states[this.state];

        this.state = (this.state + 1)%this.states.length;
        let n = this.states[this.state];
        this.transition(p, n);

    }

}


const svg_ns = 'http://www.w3.org/2000/svg';
// export { range, easeInOut };
