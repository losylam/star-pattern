const h_hexa = Math.sqrt(3)/2;
const patterns = {};

patterns.hexa = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h;
        let step_y = s+s/2;
        return [step_x, step_y];
    },

    offset: true,

    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        return [
            [0, -s],     // 0 // hex1 1
            [h, -s/2],   // 1
            [h, s/2],    // 2
            [0, s],      // 3
            [-h, s/2],   // 4
            [-h, -s/2],  // 5
        ];
    },
    polys: ()=>{
        return [
            [0, 1, 2, 3, 4, 5], // hex1
        ];
    },

    indices : (i0, xm, xp, ym, yp) => {
        return [
        ];
    },

    hankins : (i0, xm, xp, ym, yp) => {
        return [
            {i: i0+2, pts: [i0+3, i0+1, yp+1]},
            {i: i0+3, pts: [i0+4, i0+2, yp+4]},
        ];
    }
};

patterns.truncated_square = {
    steps: params => {
        let s = params.size;
        let step_x = 2*s + s*Math.SQRT2;
        let step_y = s/4 + s*Math.SQRT2;
        return [step_x, step_y];
    },

    offset: true,

    pts: params => {
        let s = params.size;
        let size = map(params.pos, 1, -1, s/2, s*(1+ Math.SQRT2)/2);
        return [
            [-size, -size],     // 0 // hex1 1
            [size, -size],   // 1
            [size, size],    // 2
            [-size, size],    // 2
        ];
    },
    polys: ()=>{
        return [
            [0, 1, 2, 3, 0], // hex1
        ];
    },

    indices : (i0, xm, xp, ym, yp) => {
        return [
            [i0+2, yp+0],
            [i0+3, ym+1]

        ];
    },

    hankins : (i0, xm, xp, ym, yp) => {
        return [
            {i: i0+2, pts: [i0+3, i0+1, yp+0]},
            {i: i0+3, pts: [i0+0, i0+2, ym+1]},
            {i: yp+0, pts: [yp+1, yp+3, i0+2]},
            {i: ym+1, pts: [ym+2, ym+0, i0+3]},
            // {i: i0+3, pts: [i0+4, i0+2, yp+4]},
        ];
    }

};

patterns.square_star = {
    steps: params => {
        let s = params.size;
        let step_x = s * (1 + Math.sqrt(3)/ 2);
        let step_y = s * (1 + Math.sqrt(3)/ 2);
        if (Math.abs(params.pos-1) > 0.02){
            step_x = map(params.pos, 1, 0, step_x, 2*s);
            step_y = map(params.pos, 1, 0, step_y, 2*s);
        }
        return [step_x, step_y, -s/2 * params.pos, s/2*params.pos];
    },

    offset: false,

    pts: params => {
        let size = params.size/2;
        return [
            [-size, -size],     // 0 // hex1 1
            [size, -size],   // 1
            [size, size],    // 2
            [-size, size],    // 2
        ];
    },

    polys: ()=>{
        return [
        ];
    },

    indices : (i0, xm, xp, ym, yp) => {
        return [
            [i0+0, i0+1, i0+2, i0+3, i0+0],
            [i0+1, xp+0, i0+2, i0+1],
            [xp+3, i0+2, xp+0, xp+3],
            [i0+2, yp+1, i0+3, i0+2],
            [yp+0, i0+3, yp+1, yp+0],
            [i0+3, yp+0, ym+1, xm+2, i0+3]
        ];
    },

    hankins : (i0, xm, xp, ym, yp) => {
        return [
            {i: i0+3, pts: [i0+ 0, i0+2, yp+1, yp+0, xm+2]},
            {i: yp+0, pts: [i0+ 3, yp+1, yp+3, ym+2, ym+1]},
            {i: xm+2, pts: [i0+0, i0+3, ym+1, xm+3, xm+1]},
            {i: ym+1, pts: [xm+3, xm+2, yp+0, ym+2, ym+0]},

            // {i: i0+3, pts: [i0+4, i0+2, yp+5, ym+1]},
            // {i: i0+4, pts: [i0+5, i0+3, ym+0, xm+2]},
            // {i: yp+0, pts: [yp+1, yp+5, i0+2, xp+4]},
            // {i: yp+5, pts: [yp+0, yp+4, ym+1, i0+3]},
            // {i: ym+1, pts: [i0+3, yp+5, ym+2, ym+0]},
        ];
    }
};

patterns.rhombitrihex = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h+s;
        let step_y = s + s/2+h;
        return [step_x, step_y];
    },
    offset: true,

    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        return [
            [0, -s],     // 0 // hex1 1
            [h, -s/2],   // 1
            [h, s/2],    // 2
            [0, s],      // 3
            [-h, s/2],   // 4
            [-h, -s/2],  // 5
        ];
    },
    polys: ()=>{
        return [
            [0, 1, 2, 3, 4, 5], // hex1
        ];
    },

    indices : (i0, xm, xp, ym, yp) => {
        return [
            [i0+1, xp+5, xp+4, i0+2],
            [i0+2, xp+4, yp],
            [i0+2, yp+0, yp+5, i0+3],
            [i0+3, yp+5, ym+1],
            [i0+3, ym+1, ym, i0+4]
        ];
    },

    hankins : (i0, xm, xp, ym, yp) => {
        return [
            {i: i0+2, pts: [i0+3, i0+1, xp+4, yp+0]},
            {i: i0+3, pts: [i0+4, i0+2, yp+5, ym+1]},
            {i: i0+4, pts: [i0+5, i0+3, ym+0, xm+2]},
            {i: yp+0, pts: [yp+1, yp+5, i0+2, xp+4]},
            {i: yp+5, pts: [yp+0, yp+4, ym+1, i0+3]},
            {i: ym+1, pts: [i0+3, yp+5, ym+2, ym+0]},
        ];
    }
};

patterns.n5 = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h+2*s + s*params.pos;
        let step_y = s+s/2+2*h + h*params.pos;
        return [step_x, step_y];
    },
    offset: true,

    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        return [
            [0, -s],     // 0 // hex1 1
            [h, -s/2],   // 1
            [h, s/2],    // 2
            [0, s],      // 3
            [-h, s/2],   // 4
            [-h, -s/2],  // 5

            [h+s, -s/2], // 6 // sq1 1
            [h+s,  s/2], // 7 sq1 2

            [h+s/2, s/2+h], // 8 //tr1 2

            [s/2, h+s], // 9 // sq2 3

            [-s/2, h+s],// 10 //tr2

            [-h-s/2, s/2+h], // 11 // sq3

            [-s-h, s/2], // 12 //tr3

            [-s-h, -s/2], // 13 // sq4

            [-h-s/2, -s/2-h], // tr4

            [-s/2, -s-h], //sq5

            [s/2, -s-h], //tr5

            [h+s/2, -s/2-h], // sq6
        ];
    },

    polys: ()=>{
        return [
            [0, 1, 2, 3, 4, 5, 0], // hex1
            // [6, 7, 2], // sq1
            [7, 8, 2, 7], // tr1
            // [2, 8, 9, 3], // sq2
            [3, 9, 10, 3], // tr2
            // [3, 10, 11, 4], // sq3
            [4, 11, 12, 4], // tr3
            // [5, 4, 12, 13], // sq4
            [14, 5, 13, 14], //tr4
            // [15, 0, 5, 14], //sq5
            [16, 0, 15, 16], //tr5
            // [16, 17, 1, 0], //sq6
            [17, 6, 1, 17]
        ];
    },

    indices : (i0, xm, xp, ym, yp) => {
        return [
            [xp+13, xp+12, i0+7, i0+6, xp+13],
            [i0+8, yp+15, yp+14, i0+9, i0+8],
            [i0+10, ym+17, ym+16, i0+11, i0+10]
        ];
    },

    hankins : (i0, xm, xp, ym, yp) => {
        return [
            {i: i0+0, pts: [i0+1, i0+5, i0+15, i0+16]},
            {i: i0+1, pts:  [i0+2, i0+0, i0+17, i0+6]},
            {i: i0+2, pts:  [i0+3, i0+1, i0+7, i0+8]},
            {i: i0+3, pts:  [i0+4, i0+2, i0+9, i0+10]},
            {i: i0+4, pts:  [i0+5, i0+3, i0+11, i0+12]},
            {i: i0+5, pts:  [i0+0, i0+4, i0+13, i0+14]},
            {i: i0+6, pts:  [xp+13, i0+7, i0+1, i0+17]},
            {i: i0+7, pts:  [xp+12, i0+8, i0+2, i0+6]},
            {i: i0+8, pts:  [yp+15, i0+9, i0+2, i0+7]},
            {i: i0+9, pts:  [yp+14, i0+10, i0+3, i0+8]},
            {i: i0+10, pts:  [ym+17, i0+11, i0+3, i0+9]},
            {i: i0+11, pts:  [ym+16, i0+12, i0+4, i0+10]},
            {i: xp+12, pts:  [xp+4, xp+11, i0+7, xp+13]},
            {i: xp+13, pts:  [xp+5, xp+12, i0+6, xp+14]},
            {i: yp+14, pts:  [yp+15, yp+5, yp+13, i0+9]},
            {i: yp+15, pts:  [yp+16, yp+0, yp+14, i0+8]},
            {i: ym+16, pts:  [ym+17, ym+0, ym+15, i0+11]},
            {i: ym+17, pts:  [ym+6, ym+1, ym+16, i0+10]},

            // plus
            // {i: i0+0, pts: [i0+1, i0+3, i0+5, i0+15, i0+16]},
            // {i: i0+1, pts: [i0+2, i0+4, i0+0, i0+17, i0+6]},
            // {i: i0+2, pts: [i0+3, i0+5, i0+1, i0+7, i0+8]},
            // {i: i0+3, pts: [i0+4, i0+0, i0+2, i0+9, i0+10]},
            // {i: i0+4, pts: [i0+5, i0+1, i0+3, i0+11, i0+12]},
            // {i: i0+5, pts: [i0+0, i0+2, i0+4, i0+13, i0+14]},
        ];
    }
};

patterns.n9 = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h+2*s;
        let step_y = s+s/2+2*h;
        return [step_x, step_y];
    },
    offset: true,
    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        return [
            [s/2, -h],
            [s, 0],
            [s/2, h],
            [-s/2, h],
            [-s, 0],
            [-s/2, -h],

            [s/2, -h-s],
            [s/2+h, -h-s/2],

            [s+h, -s/2],
            [s+h, s/2],

            [s/2+h, h+s/2],
            [s/2, h+s],

            [-s/2, h+s],
            [-s/2-h, h+s/2],

            [-s-h, s/2],
            [-s-h, -s/2],

            [-s/2-h, -h-s/2],
            [-s/2, -h-s],
        ];
    },

    polys: ()=>{
        return [
            [0, 5, 17, 6, 0],
            [1, 0, 7, 8, 1],
            [2, 1, 9, 10, 2],
            [3, 2, 11, 12, 3],
            [4, 3, 13, 14, 4],
            [5, 4, 15, 16, 5],
            [8, 9],
            [10, 11],
            [12, 13]
        ];
    },

    indices : (i0, xm, xp, ym, yp) => {
        return [
        ];
    },

    hankins : (i0, xm, xp, ym, yp) => {
        return [
            {i: i0+0, pts: [i0+1, i0+5, i0+6, i0+7]},
            {i: i0+1, pts: [i0+2, i0+0, i0+8, i0+9]},
            {i: i0+2, pts: [i0+3, i0+1, i0+10, i0+11]},
            {i: i0+3, pts: [i0+4, i0+2, i0+12, i0+13]},
            {i: i0+4, pts: [i0+5, i0+3, i0+14, i0+15]},
            {i: i0+5, pts: [i0+0, i0+4, i0+16, i0+17]},

            {i: i0+9, pts: [i0+10, i0+1, i0+8, xp+4, xp+13]},
            {i: i0+10, pts: [i0+11, i0+2, i0+9, xp+13, yp+5]},
            {i: i0+11, pts: [i0+12, i0+2, i0+10, yp+5, yp+15]},
            {i: i0+12, pts: [i0+13, i0+3, i0+11, yp+15, ym+0]},
            {i: i0+13, pts: [i0+14, i0+3, i0+12, ym+0, ym+17]},
            {i: i0+15, pts: [i0+16, i0+4, i0+14, xm+1, xm+7]},
        ];
    }
};

patterns.trunc_hex = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h+2*s;
        // let step_y = 2*h+3*s + params.pos*1.5*s;
        let step_y = 1.5*s+2*h;
        return [step_x, step_y];
    },
    offset: true,
    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        let dx = params.pos*h;
        let dy = params.pos*s/2;
        return [

            [-s/2, -h-s],    // 0 //quad 1
            [s/2, -h-s],     // 1


            [s/2+h, -h-s/2], // 4  // quad 2
            [s+h, -s/2],     // 5


            [s+h, s/2], // quad 3
            [s/2+h, h+s/2],

            [s/2, h+s], // quad 4
            [-s/2, h+s],


            [-s/2-h, h+s/2],
            [-s-h, s/2],

            [-s-h, -s/2],
            [-s/2-h, -h-s/2]
        ];
    },
    polys: ()=>{
        return [
            // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0]
            [1, 2],
            [3, 4],
            [5, 6],
            // [7, 8],
            // [9, 10],
            // [11, 0],
        ];
    },
    indices: (i0, xm, xp, ym, yp)=>[
        [i0+4, i0+5, xp+8, i0+4],
        [i0+7, i0+6, yp+10, i0+7],

    ],
    hankins: (i0, xm, xp, ym, yp)=>{
        return [
            {i: i0+2, pts: [i0+3, i0+1, xp+11]},
            {i: i0+3, pts: [i0+4, i0+2, xp+11]},
            {i: i0+4, pts: [i0+5, i0+3, xp+8]},
            {i: i0+5, pts: [i0+6, i0+4, xp+8]},
            {i: i0+6, pts: [i0+7, i0+5, ym+3]},
            {i: i0+8, pts: [i0+9, i0+7, ym+0]},
            // {i: i0+1, pts: [i0+4, i0+0, i0+2]},

        ];
    }
};

patterns.trihex = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*s;
        let step_y = 2*h;
        return [step_x, step_y];
    },
    offset: true,
    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        return [
            [-s/2, -h],
            [s/2, -h],
            [s,0],
            [s/2, h],
            [-s/2, h],
            [-s,0],

        ];
    },
    polys: ()=>{
        return [
            [0, 1, 2, 3, 4, 5, 0]
        ];
    },
    indices: (i0, xm, xp, ym, yp)=>[
        // [i0+4, xp+9, xp+8, yp+1, yp+0, i0+5, i0+4],
        // [i0+6, yp+11, yp+10, ym+3, ym+2, i0+7, i0+6],
        // [i0+7, i0+6, yp+10, i0+7],

    ],
    hankins: (i0, xm, xp, ym, yp)=>{
        return [
            {i: i0+2, pts: [i0+3, i0+1, xp+0, xp+4]},
            {i: i0+3, pts: [i0+4, i0+2, yp+1, yp+5]},
            {i: i0+4, pts: [i0+5, i0+3, ym+2, ym+0]},
            // {i: i0+6, pts: [i0+7, i0+5, ym+3]},
            // {i: i0+8, pts: [i0+9, i0+7, ym+0]},
            // {i: i0+1, pts: [i0+4, i0+0, i0+2]},

        ];
    }
};

patterns.trunc_trihex = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h+3*s;
        // let step_y = 2*h+3*s + params.pos*1.5*s;
        let step_y = 1.5*s+3*h;
        return [step_x, step_y];
    },
    offset: true,
    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        let dx = params.pos*h;
        let dy = params.pos*s/2;
        return [

            [-s/2, -h-s],    // 0 //quad 1
            [s/2, -h-s],     // 1


            [s/2+h, -h-s/2], // 4  // quad 2
            [s+h, -s/2],     // 5


            [s+h, s/2], // quad 3
            [s/2+h, h+s/2],

            [s/2, h+s], // quad 4
            [-s/2, h+s],


            [-s/2-h, h+s/2],
            [-s-h, s/2],

            [-s-h, -s/2],
            [-s/2-h, -h-s/2]
        ];
    },
    polys: ()=>{
        return [
            // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0],
            [1, 2],
            [3, 4],
            [5, 6],
            [7, 8],
            [9, 10],
            [11, 0]
        ];
    },
    indices: (i0, xm, xp, ym, yp)=>[
        [i0+4, xp+9, xp+8, yp+1, yp+0, i0+5, i0+4],
        [i0+6, yp+11, yp+10, ym+3, ym+2, i0+7, i0+6],
        // [i0+7, i0+6, yp+10, i0+7],

    ],
    hankins: (i0, xm, xp, ym, yp)=>{
        return [
            {i: i0+3, pts: [i0+4, i0+2, xp+10]},
            {i: i0+4, pts: [i0+5, i0+3, xp+9]},
            {i: i0+5, pts: [i0+6, i0+4, yp+0]},
            {i: i0+6, pts: [i0+7, i0+5, yp+11]},
            {i: i0+7, pts: [i0+8, i0+6, ym+2]},
            {i: i0+8, pts: [i0+9, i0+7, ym+1]},
            {i: i0+9, pts: [i0+10, i0+8, xm+4]},
            {i: i0+10, pts: [i0+11, i0+9, xm+3]},
            {i: yp+2, pts: [yp+3, yp+1, xp+7]},
            {i: yp+1, pts: [yp+2, yp+0, xp+8]},
            {i: yp+0, pts: [yp+1, yp+11, i0+5]},
            {i: yp+11, pts: [yp+0, yp+10, i0+6]},
            // {i: i0+5, pts: [i0+6, i0+4, xp+8]},
            // {i: i0+6, pts: [i0+7, i0+5, ym+3]},
            // {i: i0+8, pts: [i0+9, i0+7, ym+0]},
            // {i: i0+1, pts: [i0+4, i0+0, i0+2]},

        ];
    }
};

patterns.n16 = {
    steps: params => {
        let s = params.size;
        let h = h_hexa*s;
        let step_x = 2*h+2*s + params.pos*2*h;
        // let step_y = 2*h+3*s + params.pos*1.5*s;
        let step_y = 1.5*s+2*h + params.pos*1.5*s;
        return [step_x, step_y];
    },
    offset: true,
    pts: params => {
        let s = params.size;
        let h = h_hexa*s;
        let dx = params.pos*h;
        let dy = params.pos*s/2;
        return [

            [-s/2, -h-s],    // 0 //quad 1
            [s/2, -h-s],     // 1


            [s/2+h, -h-s/2], // 4  // quad 2
            [s+h, -s/2],     // 5


            [s+h, s/2], // quad 3
            [s/2+h, h+s/2],

            [s/2, h+s], // quad 4
            [-s/2, h+s],


            [-s/2-h, h+s/2],
            [-s-h, s/2],

            [-s-h, -s/2],
            [-s/2-h, -h-s/2],

            [s/2 + h + dx, h+s/2+ dy], // 12
            [s+h + dx, s/2 + dy],     // 13

            [s/2, h+s+params.pos*s],   // 14
            [s/2-s, h+s+params.pos*s], // 15


            [-s/2+2*s+h+dx, h+s/2 + dy],    // 0 //quad 1
            [0, 2*h+s+params.pos*s],
        ];
    },
    polys: ()=>{
        return [
            // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0]
            [1, 2],
            [3, 4],
            [5, 6],
            [7, 8],
            [9, 10],
            [11, 0],
            [4, 5, 12, 13, 4],
            [6, 7, 15, 14, 6]
        ];
    },
    indices: (i0, xm, xp, ym, yp)=>[
        [i0 + 12, i0+16, yp+1, yp+0, i0+12],
        [i0 + 13, i0+16, xp+8, xp+9, i0+13],
        [i0 + 14, i0+17, yp+10, yp+11, i0+14],
        [i0 + 15, ym+2, ym+3, i0+17, i0+15],

    ],
    hankins: (i0, xm, xp, ym, yp)=>{
        return [
            {i: i0+4, pts: [i0+3, i0+13, i0+5]},
            {i: i0+5, pts: [i0+4, i0+12, i0+6]},
            {i: i0+6, pts: [i0+5, i0+14, i0+7]},
            {i: i0+7, pts: [i0+6, i0+15, i0+8]},
            {i: i0+13, pts: [i0+4, xp+9, i0+16, i0+12]},
            {i: i0+12, pts: [i0+5, i0+13, i0+16, yp+0]},
            {i: i0+14, pts: [i0+6, yp+11, i0+17, i0+15]},
            {i: i0+15, pts: [i0+7, i0+14, i0+17, ym+2]},
            {i: i0+16, pts: [i0+12, i0+13, xp+8, yp+1]},
            {i: i0+17, pts: [i0+15, i0+14, yp+10, ym+3]},
            {i: i0+8, pts: [i0+9, i0+7, xm+16]},
            {i: i0+9, pts: [i0+10, i0+8, xm+13]},
            {i: yp+0, pts: [yp+11, i0+12, yp+1]},
            {i: yp+1, pts: [yp+0, i0+16, yp+2]},
            {i: yp+10, pts: [yp+9, i0+17, yp+11]},
            {i: yp+11, pts: [yp+10, i0+14, yp+0]},
            {i: ym+2, pts: [ym+1, i0+15, ym+3]},
            {i: ym+3, pts: [ym+2, i0+17, ym+4]},

            // {i: i0+0, pts: [i0+1, i0+15, i0+3]},
            // {i: i0+1, pts: [i0+4, i0+0, i0+2]},

        ];
    }
};

export default patterns;
